from random import randint
import sys, time

def loading_dots():
	sys.stdout.write("Rolling")
	for i in range(6):
	    x = i % 2
	    sys.stdout.write("." * x)
	    time.sleep(0.3)
	    sys.stdout.flush()

if __name__ == '__main__':

	while True:
		print("\nWelcome to the Dice Rolling Simulator!" 
		      "\nEnter the lowest and highest values for each dice.")

		while True:
			try:
				low = int(raw_input("Lowest value: "))
				high = int(raw_input("Highest value: "))
				while (high < low):
					print ("Highest value less than lowest value(%d)!" % low)
					high = int(raw_input("Enter a new highest value: "))
				break
			except ValueError:
				print ("Oops! That was not a valid number. Try again...")

		num = randint(low, high)

		loading_dots()

		print("\nYou rolled %d!" % num)

		user_decision = raw_input("Would you like to play again? (y/n): ").lower()

		if user_decision.startswith('y'):
			continue
		else:
			break


